# Manage

Welcome! This is the repo for all things related to the [Manage stage](https://about.gitlab.com/handbook/product/categories/#devops-stages) of the DevOps lifecycle.

## Resources

- [Product vision](https://about.gitlab.com/direction/manage/)
- [Manage Frontend Engineering](https://about.gitlab.com/handbook/engineering/frontend/manage/)
- [Manage Backend Engineering](https://about.gitlab.com/handbook/engineering/dev-backend/manage/)

## Filing Issues

If you'd like to file an issue for GitLab CE or EE that the Manage team should 
look into, please file an issue in the desired project:

- [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce)
- [GitLab EE](https://gitlab.com/gitlab-org/gitlab-ee)

Please use the ~Manage and ~"devops:manage" labels so we can find it quickly!

[Manage stage group]: https://about.gitlab.com/handbook/product/categories/manage/
[Product vision]: https://about.gitlab.com/direction/manage/
[Manage Frontend Engineering]: https://about.gitlab.com/handbook/engineering/frontend/manage/