## {REPLACE_WITH_MILESTONE} Planning for {REPLACE_WITH_GROUP}

* Board link: {REPLACE_WITH_BOARD_LINK}

--------------------------------------------------------------------------------

## Objectives

<!-- Problems we're trying to solve in this release and why they matter. -->

## Defining Success

<!-- The metrics or other data points you're using in this release to guide priorities. -->
<!-- Prioritized issues should always consider IACV, your group's North Star, and your group's direction/vision. -->
<!-- e.g. "These features support progress towards Dev Section's Top 2 IACV Q1FY21 goals." -->

## To-Do
- [ ] Define an approximate scope for the coming release.
- Apply appropriate labels to issues:
  - [ ] Priority
  - [ ] Department
  - [ ] Pricing Tier
- [ ] Review the proposed scope with Engineering.
- [ ] Review design objectives with UX.
- [ ] Get feedback and estimates from Engineering, confirming scope for the release.
- [ ] Final verification of issues scheduled for the release.
- [ ] Present selected issues in the Manage Kickoff on the 17th!

/label ~"Planning Issue" 
